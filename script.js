const gameContainer = document.getElementById("game");
const startBtn = document.getElementById("start-btn");
const restartBtn = document.getElementById("restart-btn");
const type = document.getElementById("type");
const difficulty = document.getElementById("difficulty");
const winner = document.getElementById("winner");
const movesDisplay = document.getElementById("moves");
const choices = document.querySelector(".choices");
// const result = document.getElementById("highscore");
const labels = document.getElementById("show-difficulty");
const typeShow = document.getElementById("show-types");
const maxScore = document.getElementById("max-score");
const sfx = document.getElementById("sfx");

let COLORS = [
	"red",
	"blue",
	"green",
	"orange",
	"red",
	"blue",
	"green",
	"orange",
];

let GIF = [
	"1.gif",
	"2.gif",
	"3.gif",
	"4.gif",
	"1.gif",
	"2.gif",
	"3.gif",
	"4.gif",
];

// Event Listener for Start Button
startBtn.addEventListener("click", () => {
	sfx.autoplay = "true";
	sfx.load();
	let gameType;
	console.log(gameContainer.style);
	if (gameContainer.style.display == "flex") {
		gameContainer.style.display = "none";
	} else {
		gameContainer.style.display = "flex";
	}
	let finalMax;
	startBtn.style.display = "none";
	labels.style.display = "none";
	typeShow.style.display = "none";
	restartBtn.style.display = "block";

	maxScore.innerText = `Best Score : 0`;
	if (type.value === "colors") {
		gameType = 1;
	} else {
		gameType = 2;
	}
	if (difficulty.value == 1) {
		finalMax = 8;
		COLORS = [
			"red",
			"blue",
			"green",
			"orange",
			"red",
			"blue",
			"green",
			"orange",
		];

		GIF = [
			"1.gif",
			"2.gif",
			"3.gif",
			"4.gif",
			"1.gif",
			"2.gif",
			"3.gif",
			"4.gif",
		];
	} else if (difficulty.value == 2) {
		finalMax = 12;
		COLORS = [
			"red",
			"blue",
			"green",
			"orange",
			"black",
			"gray",
			"red",
			"blue",
			"green",
			"orange",
			"black",
			"gray",
		];
		GIF = [
			"1.gif",
			"2.gif",
			"3.gif",
			"4.gif",
			"5.gif",
			"6.gif",
			"1.gif",
			"2.gif",
			"3.gif",
			"4.gif",
			"5.gif",
			"6.gif",
		];
	} else {
		finalMax = 16;
		COLORS = [
			"red",
			"blue",
			"green",
			"orange",
			"black",
			"gray",
			"yellow",
			"pink",
			"red",
			"blue",
			"green",
			"orange",
			"black",
			"gray",
			"yellow",
			"pink",
		];
		GIF = [
			"1.gif",
			"2.gif",
			"3.gif",
			"4.gif",
			"5.gif",
			"6.gif",
			"7.gif",
			"8.gif",
			"1.gif",
			"2.gif",
			"3.gif",
			"4.gif",
			"5.gif",
			"6.gif",
			"7.gif",
			"8.gif",
		];
	}
	let shuffledColors;
	let shuffledGif;
	if (gameType == 1) {
		shuffledColors = shuffle(COLORS);
	} else {
		shuffledGif = shuffle(GIF);
	}
	if (gameType == 1) {
		console.log(shuffledColors);
		createDivsForColors(shuffledColors);
	} else {
		console.log("this");
		createDivsForGifs(shuffledGif);
	}
	choices.style.display = "none";
	movesDisplay.style.display = "flex";
	if (JSON.parse(localStorage.getItem("highestScore")) != undefined) {
		maxScore.innerText = `Best Score: ${localStorage.getItem("highestScore")}`;
	}

	let clickCards = [];
	let moves = 0;

	let cardColored = 0;
	// TODO: Implement this function!
	function handleCardClick(event) {
		if (event.target.classList[1] !== "clicked") {
			if (clickCards.length < 2) {
				moves += 1;
				if (gameType == 1) {
					console.log(event.target.className);
					event.target.style = `background-color:${event.target.className}; transform : rotateY(-180deg); transition : 300ms ease-in-out`;
				} else {
					event.target.style = `background-image:url(gifs/${event.target.className}); background-size: cover;					`;
				}
				event.target.classList.add("clicked");
				clickCards.push(event.target);
			}
			if (clickCards.length === 2) {
				if (clickCards[0].classList[0] !== clickCards[1].classList[0]) {
					setTimeout(() => {
						if (gameType == 1) {
							clickCards[0].style = `background-color:darkolivegreen; transform : rotateY(180deg); transition : 100ms ease-in-out`;
							clickCards[1].style = `background-color: darkolivegreen;transform : rotateY(180deg); transition : 100ms ease-in-out ;`;
						} else {
							clickCards[0].style = `background-image: none;`;
							clickCards[1].style = `background-image: none;`;
						}
						clickCards[0].classList.remove("clicked");
						clickCards[1].classList.remove("clicked");

						clickCards = [];
					}, 1000);
				} else {
					const div1 = clickCards[0];
					const div2 = clickCards[1];
					div1.removeEventListener("click", handleCardClick);
					div2.removeEventListener("click", handleCardClick);
					cardColored += 2;

					clickCards = [];
				}
			}
		}
		movesDisplay.innerText = `Moves = ${moves.toString()}`;
		isWinner(cardColored);
		console.log("Moves =" + moves);
	}
	restartBtn.addEventListener("click", () => {
		location.reload(true);
	});
	function shuffle(array) {
		let counter = array.length;

		// While there are elements in the array
		while (counter > 0) {
			// Pick a random index
			let index = Math.floor(Math.random() * counter);

			// Decrease counter by 1
			counter--;

			// And swap the last element with it
			let temp = array[counter];
			array[counter] = array[index];
			array[index] = temp;
		}
		return array;
	}

	function createDivsForColors(colorArray) {
		for (let color of colorArray) {
			// create a new div
			const newDiv = document.createElement("div");

			// give it a class attribute for the value we are looping over
			newDiv.classList.add(color);

			// call a function handleCardClick when a div is clicked on

			newDiv.addEventListener("click", handleCardClick);
			// append the div to the element with an id of game
			gameContainer.append(newDiv);
		}
	}

	function createDivsForGifs(gifArray) {
		for (let gif of gifArray) {
			// create a new div
			const newDiv = document.createElement("div");

			// give it a class attribute for the value we are looping over
			newDiv.classList.add(gif);

			// call a function handleCardClick when a div is clicked on

			newDiv.addEventListener("click", handleCardClick);
			// append the div to the element with an id of game
			gameContainer.append(newDiv);
		}
	}

	//Winner Check
	function isWinner(cardColored) {
		if (cardColored == finalMax) {
			setTimeout(() => {
				gameContainer.style.display = "none";
				restartBtn.style.display = "block";
				winner.innerText = `GAME OVER!`;
				winner.style =
					"font-size: 2em; color:chocolate ; text-align : center; margin: 1rem";
				checkHighScore(moves);
			}, 1 * 1000);
		}
	}

	//High Score Check
	function checkHighScore(moves) {
		if (JSON.parse(localStorage.getItem("highestScore")) != undefined) {
			if (localStorage.getItem("highestScore") > moves) {
				localStorage.setItem("highestScore", moves);
			} else {
			}
		} else {
			maxScore.innerText = `Best Score : ${moves}`;
			localStorage.setItem("highestScore", moves);
		}
	}
});

//Event Listener for Restart Button

// when the DOM lolet finalMax;ads

// function handleCardClick(event) {
// 	// you can use event.target to see which element was clicked
// 	if (event.target.classList[1] !== "clicked") {
// 		if (clickCards.length < 2) {
// 			event.target.style = `background-color:${event.target.className}; transform : rotateY(-180deg); transition : 300ms ease-in-out`;
// 			event.target.classList.add("clicked");
// 			clickCards.push(event.target);
// 		}
// 		if (clickCards.length === 2) {
// 			if (clickCards[0].classList[0] !== clickCards[1].classList[0]) {
// 				setTimeout(() => {
// 					clickCards[0].style = `background-color:yellowgreen; transform : rotateY(180deg); transition : 300ms ease-in-out`;
// 					clickCards[1].style = `background-color: yellowgreen;transform : rotateY(180deg); transition : 300ms ease-in-out ;`;
// 					clickCards[0].classList.remove("clicked");
// 					clickCards[1].classList.remove("clicked");
// 					moves += 2;
// 					clickCards = [];
// 				}, 1000);
// 			} else {
// 				const div1 = clickCards[0];
// 				const div2 = clickCards[1];
// 				div1.removeEventListener("click", handleCardClick);
// 				div2.removeEventListener("click", handleCardClick);
// 				cardColored += 2;
// 				moves += 2;
// 				scoreCard.innerText = cardColored.toString();
// 				clickCards = [];
// 			}
// 		}
// 	}
// 	console.log("Moves =" + moves / 2);
// }
